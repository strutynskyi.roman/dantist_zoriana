// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';
// ------------------------------
// Imports
// ------------------------------

import $ from 'jquery';
// export for others scripts to use
window.$ = $;

// require('waypoint/lib/waypoint.js');
require('waypoints/lib/noframework.waypoints.js');
require('slick-carousel/slick/slick.js');
// Modules
import Link from './../_modules/molecules/menu/menu';

// Organisms
import Header from './../_modules/organisms/header/header'
import Main from './../_modules/organisms/main/main'


//Molecules 
import Slider from './../_modules/molecules/slider/slider'
import Modal from './../_modules/molecules/modal-window/modal-window'
import Service from './../_modules/molecules/services/services'
import HeaderSlider from './../_modules/molecules/img-header/img-header'
import News from './../_modules/molecules/new/new'
import ContactUs from './../_modules/molecules/contact-us/contact-us'
import Teams from './../_modules/molecules/teams/teams'


// import Tabs from './../_modules/molecules/tabby/tabby'  


// ------------------------------
// Additional functionality
// ------------------------------

// Crossbrowser plugin for icons
import svg4everybody from 'svg4everybody/dist/svg4everybody.legacy.js';
svg4everybody();

// Data pages
let currentPage = $('body').data('page');

// ------------------------------
// Initialization modules
// ------------------------------

$(() => {
  // Common scripts

  // Home page scripts
  if (currentPage === 'home') {
  new Service();
  new Slider();
  new Header();
  new HeaderSlider();
  new Main();
  new Modal();
  new ContactUs();
  new Link();
  new Teams();
  }
});
