'use strict';

import jqueryValidation from 'jquery-validation'
export default class ModalWindow {
  constructor() {
    let self = this;
    // this.modalSelector = '.js-modal';    
    // this.modalOverlay = '.js-modal-overlay';
    this.modal = '.js-modal-form';
    // this.btnOpen = '.js-modal-open';
    // this.btnClose = '.js-modal-cross';

    this.initOrder();

    // console.log('%s module', this.orderSelector.toLowerCase());
  }
  initOrder() {
    let self = this;
    let captchaImg = $(this.modal).find('.captcha').addClass('modal-window__captcha-img');
    let captchaInput = $(this.modal).find('#id_captcha_1').addClass('field__input');
    let captchaLabel = $(this.modal).find('.field__label-captcha, #id_captcha_1').wrapAll("<div class='field modal-window__field-captcha'></div>");
    let captchaInputHidden = $(this.modal).find('.captcha, .modal-window__field-captcha, #id_captcha_0').wrapAll("<div class='modal-window__captcha'></div>");
    $(captchaImg).click(() => {
      this.refrechCaptcha();
    });
    $(document).ready(function () {
      function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
          let cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
            let cookie = $.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === name + '=') {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
            }
          }
        }
        return cookieValue;
      }
      $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
          if (regexp.constructor != RegExp)
            regexp = new RegExp(regexp);
          else if (regexp.global)
            regexp.lastIndex = 0;
          return this.optional(element) || regexp.test(value);
        },
        "Please check your input."
      );
      let currentLang = $('body').data('lang');
      if(currentLang === 'en'){
        $(self.modal).validate({
          rules: {
            fullName: {
              required: true,
              minlength: 2
            },
            mail: {
              email: true,
              required: true,
              regex: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z0-9_]+/
            },
            message: {
              required: true
            },
            captcha_1: {
              required: true,
              minlength: 4,
              maxlength: 4,
            }
          },
          messages: {
            fullName: {
              required: "Typing some first name",
              minlength:"The first name is too short"
            },
            mail: {
              email: "Wrong email",
              required: "Typing some email",
              regex: 'Enter a valid email '
            },
            message: {
              required: "Enter your message"
            },
            captcha_1: {
              required: "Enter the captcha",
              minlength: "Your captcha is too short",
              maxlength: "Your captcha is too long"
            }
          },
          onclick: true,
          errorClass: "error",
          validClass: "valid",
          highlight: function (element, errorClass, validClass) {
            $(element).parent().children().addClass('error').removeClass(validClass);
          },
          unhighlight: function (element, errorClass, validClass) {
            $(element).parent().children().removeClass('error').addClass(validClass);
          }
        });
      }else{

        $(self.modal).validate({
          rules: {
            fullName: {
              required: true,
              minlength: 2
            },
            mail: {
              email: true,
              required: true,
              regex: /^[A-Za-z0-9_]+\@[A-Za-z0-9_]+\.[A-Za-z0-9_]+/
            },
            message: {
              required: true
            },
            captcha_1: {
              required: true,
              minlength: 4,
              maxlength: 4,
            }
          },
          messages: {
            fullName: {
              required: "Введіть ім'я",
              regex: "Ваше ім'я закоротке",
              minlength: "Ваше ім'я закоротке",
            },
            mail: {
              email: "Неправельний формат email адреса",
              required: "Введіть email aдрес",
              regex: 'Ваш email неправильний'
            },
            message: {
              required: "Введіть повідомлення"
            },
            captcha_1: {
              required: "Введіть капчу",
              minlength: "Ваша капча закоротка",
              maxlength: "Ваша капча задовге"
            }
          },
          onclick: true,
          errorClass: "error",
          validClass: "valid",
          highlight: function (element, errorClass, validClass) {
            $(element).parent().children().addClass('error').removeClass(validClass);
          },
          unhighlight: function (element, errorClass, validClass) {
            $(element).parent().children().removeClass('error').addClass(validClass);
          }
        });
      }

      $(self.modal).submit(function (e) {
        e.preventDefault();
        let Name = $('#fullName').val();
        let email = $('#mail').val();
        let textArea = $('#message').val();
        var that = $(this);
        if ($(this).valid()) {
          $.ajax({
            url: "/",
            method: that.attr('method'),
            data: $(this).serialize(),
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            headers: {
              'X-CSRFToken': getCookie('csrftoken'),
            },
            success: function (response) {
              if (response.message == "Invalid captcha") {
                $('img.captcha').trigger('click');
              } else {
                self.cleanModal();
              }
            }
          })
        }
      });
    });
  }
  refrechCaptcha() {
    let self = this;
    let url = location.protocol + "//" + window.location.hostname + ":" + location.port + "/captcha/refresh/";
    $.getJSON(url, {}, function (json) {
      $(self.modal).find('#id_captcha_0').val(json.key);
      $(self.modal).find('.captcha').attr('src', json.image_url);
      $(self.modal).find('#id_captcha_1').val('');
    });
    return false;
  }
  // closeModal(){
  //   let self= this;
  //   $(this.modal).removeClass('open');

  //   setTimeout(function(){
  //     $(self.modalSelector).removeClass('open');
  //   },2);
  // }
  cleanModal() {
    $(this.modal).fadeOut();
    $('.main__diagnosis .main__title').fadeOut();
    $('.main__diagnosis-text').fadeIn(500);
  }
  // closeModal(){
  //   let self= this;


  // }

}