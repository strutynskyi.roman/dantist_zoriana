'use strict';

export default class ImgHeader {
  constructor() {
    let self = this;
    this.prevArrow = '.js-header-slider-prev';
    this.nextArrow = '.js-header-slider-next';
    this.sliderHeader = '.js-header-slider';
    this.sliderHeaderItem = '.js-header-slider .slick-slide .img-header__item';
    this.init();
  }
  init () {
    let self = this;
    $(this.sliderHeader).slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      speed: 1000,
      cssEase: 'linear',
      autoplay: true,
      autoplaySpeed: 5000,
      });
      let k =$(this.sliderHeaderItem).length
      $(this.sliderHeader).on('afterChange', function(event, slick, currentSlide, nextSlide) {
        event.preventDefault();
        var i = (currentSlide ? currentSlide : 0) + 1;
        $(this).attr('data-slide' , i).find('.img-header__text[data-slide-text=' + i + ']').addClass('img-header__text--active').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        });
  });
    // Initialize events
    $(this.nextArrow).on('click', function() {
      self.sliderNext();
    });
    $(this.prevArrow).on('click', function() {
      self.sliderPrev();
    });
  }
  sliderNext () {
    $(this.sliderHeader).slick('slickNext');
  }
  sliderPrev() {
    $(this.sliderHeader).slick('slickPrev');
  }
}