'use strict';

export default class Slider {
  constructor() {
    // this.sliderTeam = ;
    // this.sliderPhoto = '.js-photo';
    // this.sliderReviews = '.js-reviews';
    
    this.prevTeam = '.js-slider-prev-team';
    this.nextTeam = '.js-slider-next-team';
    this.prevPhoto = '.js-slider-prev-photo';
    this.nextPhoto = '.js-slider-next-photo';
    this.prevReviews = '.js-slider-prev-reviews';
    this.nextReviews = '.js-slider-next-reviews';
    this.sliderTeam = $('.js-team').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      // variableWidth: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
    
    this.sliderPhoto = $('.js-photo').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      speed: 1000,
      cssEase: 'linear',
      // responsive: [
      //   {
      //     breakpoint: 768,
      //     settings: {
      //       variableWidth: true,
      //     }
      //   }
      // ]
   
      });
    this.sliderReviews = $('.js-reviews').slick({
      infinite: true,
      // vertical: true,
      
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            // variableWidth: true,
          }
        }
      ]
    });
    this.init();
  }
  init () {
    let self = this;
    // Initialize events
    $(this.nextTeam).on('click', function() {
      self.sliderNextTeam();
    });
    $(this.prevTeam).on('click', function() {
      self.sliderPrevTeam();
    });
    $(this.nextPhoto).on('click', function() {
      self.sliderNextPhoto();
    });
    $(this.prevPhoto).on('click', function() {
      self.sliderPrevPhoto();
    });
    $(this.nextReviews).on('click', function() {
      self.sliderNextReviews();
    });
    $(this.prevReviews).on('click', function() {
      self.sliderPrevReviews();
    });
  }
 sliderNextTeam () {
    $(this.sliderTeam).slick('slickNext');
  }
  sliderPrevTeam () {
    $(this.sliderTeam).slick('slickPrev');
  }
  sliderNextPhoto () {
    $(this.sliderPhoto).slick('slickNext');
  }
  sliderPrevPhoto () {
    $(this.sliderPhoto).slick('slickPrev');
  }
  sliderNextReviews () {
    $(this.sliderReviews).slick('slickNext');
  }
  sliderPrevReviews () {
    $(this.sliderReviews).slick('slickPrev');
  }
}