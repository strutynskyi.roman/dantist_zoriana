'use strict';
import jqueryValidation from 'jquery-validation';
import jqueryMask from 'jquery-mask-plugin/dist/jquery.mask.min';
export default class ModalWindow {
  constructor() {
    let self= this;
    this.contactSelector = '.js-contact-us';    
    this.contactForm = '.js-contact-us-form';
    this.contactOverlay= '.js-contact-us-overlay';
    this.contactClose= '.js-contact-us-close';
    this.contactThanks= '.contact-us__thanks';
    this.btnOpen = '.js-contact-us-open';
    
    this.initOrder();
    // console.log('%s module', this.orderSelector.toLowerCase());
  }
  initOrder(){
    
    let self= this;
    $(this.contactThanks).fadeOut();
    $(this.btnOpen).click(() => {     
        this.openModal()
    });
    $(this.contactOverlay).click(() => this.closeModal());
    $(this.contactClose).click(() => this.closeModal());
    $().ready(function() {
      $.validator.addMethod(
        "regex",
        function(value, element, regexp)
        {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
        },
        "Please check your input."
      );
      $('#phone').mask('+38(099)00-00-000');
      
      let currentLang = $('body').data('lang');
      if(currentLang === 'en'){
        $(self.contactForm).validate({
          rules: {
            fullName: {
              required: true,
              minlength: 2
              },
              phone: {
                required: true,
                regex: /^[+-]{1}[0-9]{2}\([0-9]{3}\)[0-9]{2}\-[0-9]{2}\-[0-9]{3}$/,
              }
            },
            messages: {
              fullName: {
                required: "Typing some first name",
                minlength: "The first name is too short",
              },
              phone: {
                number: "Wrong number",
                required: "Typing some number",
                regex: "e.g. +38(099)12-34-567"
              },
            },
          onclick: true,
          errorClass: "error",
          validClass: "valid",
          highlight: function( element, errorClass, validClass ) {
            $(element).parent().children().addClass('error').removeClass(validClass);
          },
          unhighlight: function( element, errorClass, validClass ) {
            $(element).parent().children().removeClass('error').addClass(validClass);
          }
        });
      }else{
        $(self.contactForm).validate({ 
          rules: {
            fullName: {
              required: true,
              minlength: 2
              },
              phone: {
                required: true,
                regex: /^[+-]{1}[0-9]{2}\([0-9]{3}\)[0-9]{2}\-[0-9]{2}\-[0-9]{3}$/,
              }
            },
            messages: {
              fullName: {
                required: "Введіть ім'я",
                minlength: "Ваше ім'я закоротке",
              },
              phone: {
                number: "Неправельний формат номера ",
                required: "Введіть номер телефону",
                regex: "напр. +38(099)12-34-567"
              },
            },
          onclick: true,
          errorClass: "error",
          validClass: "valid",
          highlight: function( element, errorClass, validClass ) {
            $(element).parent().children().addClass('error').removeClass(validClass);
          },
          unhighlight: function( element, errorClass, validClass ) {
            $(element).parent().children().removeClass('error').addClass(validClass);
          }
        });
      };
   
    
      $(self.contactForm).submit(function(e){
        
        e.preventDefault();
        var that = $(this);
        if( $(this).valid()){
          $.ajax({
            url: "/",
            method: that.attr('method'),
            data:  $( this ).serialize(),
            contentType: "application/x-www-form-urlencoded",
            dataType: "text",
          }).done(function() {
            $(self.contactForm).children().fadeOut(0);
            $(self.contactThanks).fadeIn();
            $(self.contactClose).fadeIn(0);
            
          });
        }
      });
    });
  }
  openModal(){
    let self= this;
    $(this.btnOpen).fadeOut();
    
    $(this.contactSelector).addClass('open');
    setTimeout(function(){
      $(self.contactForm).addClass('open');
    },.1);
  }
  closeModal(){
    let self= this;
    $(this.btnOpen).fadeIn();
    
    $(this.contactForm).removeClass('open');
    $(this.contactSelector).removeClass('open');
  }


}
