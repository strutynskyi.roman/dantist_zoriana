'use strict';

export default class Menu {
  constructor() {
    this.menuSelector = '.js-menu';
    this.headerSelector = '.header';
    this.overlaySelector = '.js-overlay';
    this.exitSelector = '.js-exit'
    this.hamburgerSelector = '.js-hamburger';
    this.langItem = '.lang__item'
    this.init();
  }
  init(){
    $(this.langItem).each(function () {
      let currentLang = $('body').data('lang');
      let link = $(this).data('lang-item')
      if (link === currentLang){
        let index =  $(this).addClass('lang__item--active')
      }
    })
  
    $(this.exitSelector).click(() => this.menuExit());
    $(this.overlaySelector).click(() => this.menuExit());
    $(this.hamburgerSelector).on('click',() =>  this.menuOpen());
  }
  menuOpen() {
    $(this.menuSelector).addClass('menu__open');
    $(this.overlaySelector).addClass('menu__open');
    $(this.headerSelector).addClass('header--open');
    
  }
  menuExit() {
    $(this.menuSelector).removeClass('menu__open');
    $(this.overlaySelector).removeClass('menu__open');
    $(this.headerSelector).removeClass('header--open');
    
    
      
  }
}