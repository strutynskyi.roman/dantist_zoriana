'use strict';

export default class Teams {
  constructor() {
    let self = this;
    this.root = '.js-person';
    this.visibleWrap = '.js-wrap-visible';
    this.visibleItem = '.js-visible';
    this.init();
  }
  init(){
    let self = this;

    $(this.root).on('touchstart',function(){
      let wrap =  $(this).find(self.visibleWrap);
      let item =  $(this).find(self.visibleItem);
      wrap.css('opacity','1');
      item.css('transform','translateY(0)');
    })
    $(this.root).on('touchend',function(){
      let wrap =  $(this).find(self.visibleWrap);
      let item =  $(this).find(self.visibleItem);
      wrap.removeAttr('style');
      item.removeAttr('style');
    })
  }
}
