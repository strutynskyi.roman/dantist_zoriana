'use strict';
// require('bootstrap-autohide-navbar/dist/bootstrap-autohide-navbar.js')
export default class Header {
    constructor() {
        this.headerSelector = '.header';
        this.initSelector();
    }
    initSelector() {
        //   let currentPage = $('body').data('page');
        //   if (currentPage == 'home') {
        // $(document).ready(()=>{
        //    $('a.page-scroll:last').css({
        //        border: '2px solid #1E988A',
        //         height: '40px'
        //    })
        // })
        $('a.page-scroll').bind('touchstart click', function (event) {
            var self = $(this);
            if ($('a.page-scroll').hasClass('menu__item--active')) {
                $('a.page-scroll').removeClass('menu__item--active');
                self.addClass('menu__item--active');
            } else {
                self.addClass('menu__item--active');
            }
            if ($(window).width() <= "768") {
                $('html, body').stop().animate({
                    scrollTop: $(self.attr('href')).offset().top - 40
                }, 500, function () {
                    $(".header").css({
                        background: 'white',
                        transition: '.5s',
                    });
                });
            } else {
                $('html, body').stop().animate({
                    scrollTop: $(self.attr('href')).offset().top - 75
                }, 500, function () {
                    $(".header").css({
                        background: 'white',
                        transition: '.5s',
                    });
                });
            }
            event.preventDefault();
        });
        $('a.page-scroll').on('touchend ', function () {
                $('a.page-scroll').removeClass('menu__item--active');
              
            
        });
        $(window).scroll(function () {
            var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
            var div_top = $('.header').offset().top;
            console.log()
            if ($(window).width() > "768" && $('.header').offset().top >= 40) {
                $(".header__top-line").css({
                    height: '0',
                    transition: '.5s'
                });
                $(".header").css({ background: 'rgba(255, 255, 255, 0.40)' });
                $(".img-header").css({
                    paddingTop: '65px',
                    transition: '.5s'
                });
                $(".img-header__arrow").css({
                    top: '65px',
                    transition: '.5s'
                });
            } else if ($(window).width() <= "768") {
                $(".header__top-line").css({
                    height: '40px',
                    transition: '.5s'
                });
                $(".header").css({ background: 'transparent' });
                $(".img-header").css({
                    paddingTop: '40px',
                    transition: '.5s'
                });
                $(".img-header__arrow").css({
                    top: '40px',
                    transition: '.5s'
                });

            } else {
                $(".header__top-line").css({
                    height: '40px',
                    transition: '.5s'
                });
                $(".header").css({ background: 'white' });
                $(".img-header").css({
                    paddingTop: '115px',
                    transition: '.5s'
                });
                $(".img-header__arrow").css({
                    top: '115px',
                    transition: '.5s'
                });

            }
        });
        var aChildren = $(".menu a");
        var aArray = [];
        for (var i = 0; i < aChildren.length; i++) {
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        }

        $(window).scroll(function () {
            var windowPos = $(window).scrollTop();
            var windowHeight = $(window).height();
            var docHeight = $(document).height();

            for (var i = 0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top - 100;
                var divHeight = $(theID).height();
                if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                    $("a[href='" + theID + "']").addClass("menu__item--active");
                } else {
                    $("a[href='" + theID + "']").removeClass("menu__item--active");
                }
            }
            if (windowPos + windowHeight == docHeight) {
                if (!$(".menu a:last-child ").hasClass("menu__item--active")) {
                    var navActiveCurrent = $(".menu__item--active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("menu__item--active");
                    $(".menu a:last-child").addClass("menu__item--active");
                }
            }
        });
        // }
        // if (currentPage == 'news') {
        //     $(window).width() > "768" && $(".header").bootstrapAutoHideNavbar();    
        // }    
        // if (currentPage == 'new') {
        //     $(window).width() > "768" && $(".header").bootstrapAutoHideNavbar();
        // }    
    }
}
