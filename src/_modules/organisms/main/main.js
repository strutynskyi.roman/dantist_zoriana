'use strict';

import { TimelineLite, Power4 } from 'gsap';
export default class Main {
  constructor() {
    let self = this;
    this.menuItem = '.menu__item';
    this.headerImg = '.main__home';
    this.service = '.main__services';
    this.serviceTitle = '.main__services .main__title';
    this.serviceLine = '.main__services .main__line';
    this.items = $('.services__items').children();
    this.about = '.main__about';
    this.aboutImg = '.main__about-img';
    this.aboutText = $('.main__about-block-text').children();
    this.news = '.main__news'
    this.newsTitle = '.main__news .main__title';
    this.newsLine = '.main__news .main__line';
    this.itemNews = '.new';
    this.diagnosis = '.main__diagnosis';
    this.diagnosisImg = '.main__diagnosis-block-img';
    this.diagnosisTitle = '.main__diagnosis-block-text .main__title';
    this.diagnosisModal = '.main__diagnosis-block-text .js-modal-form';
    this.reviews = '.main__reviews';
    this.reviewsTitle = '.main__reviews .main__title';
    this.reviewsLine = '.main__reviews .main__line';
    this.reviewsSlider = '.main__slider-reviews';
    this.team = '.main__team'
    this.teamTitle = '.main__team .main__title';
    this.teamLine = '.main__team .main__line';
    this.teams = $('.main__teams').children();
    // this.teamSlider = '.main__slider-photo';
    // this.menuLength = $('.menu a').attr('href');
    console.log(this.menuLength);

    this.tl = new TimelineLite();
    if($(window).width() < 580 ){
      this.waypoint = new Waypoint({
        element: $(this.headerImg).get(0),
        offset: '50%',
        handler: function (direction) {
          self.mainService();
        },
      });
    }else{
      this.waypoint = new Waypoint({
        element: $(this.service).get(0),
        offset: '50%',
        handler: function (direction) {
          self.mainService();
        },
      });
    }
    // this.waypoint = new Waypoint({
    //   element: $(this.about).get(0),
    //   offset: '50%',
    //   handler: function (direction) {
    //     self.mainAbout();
    //   },
    // });
    // this.waypoint = new Waypoint({
    //   element: $(this.news).get(0),
    //   offset: '50%',
    //   handler: function (direction) {
    //     self.mainNews();
    //   },
    // });
    // this.waypoint = new Waypoint({
    //   element: $(this.diagnosis).get(0),
    //   offset: '50%',
    //   handler: function (direction) {
    //     self.mainDiagnosis();
    //   },
    // });
    // this.waypoint = new Waypoint({
    //   element: $(this.reviews).get(0),
    //   offset: '50%',
    //   handler: function (direction) {
    //     self.mainReviews();
    //   },
    // });
    this.waypoint = new Waypoint({
      element: $(this.reviews).get(0),
      offset: '50%',
      handler: function (direction) {
        self.mainTeam();
      },
    });
    this.initServices();

  }
  initServices() {
    let self = this;
    var hellopreloader = document.getElementById("main__preloader-preload");

    function fadeOutnojquery(el){
      el.style.opacity = 1;

      var interhellopreloader = setInterval(function(){
        el.style.opacity = el.style.opacity - 0.05;
        if (el.style.opacity <=0.05){ 

          clearInterval(interhellopreloader);
          hellopreloader.style.display = "none";
          $('.js-header-slider').attr('data-slide' , 1).find('.img-header__text[data-slide-text=' + 1 + ']').addClass('img-header__text--active').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend')
          $('body').css('overflow-y','auto')

        }
      },16);
    }
    if($(window).width() < 580 ){
      Array.from(document.getElementsByClassName("js-resize-image")).forEach(element => {
        let imgThis=$(element).attr('src')
        let imgSplit = imgThis+'.phone'
          $(element).attr('src',imgSplit)
      });
      Array.from(document.getElementsByClassName("js-resize-img")).forEach(element => {
        if ($(element).is('.slider__img-reviews')) {
          $(element).removeAttr('style');
        }else{
          let imgThis= $(element).attr('style');
          let imgSplit = imgThis.split(')').join('.phone)')
          $(element).attr('style',imgSplit)
        }
      });
      window.onload = function(){
        setTimeout(function(){
          fadeOutnojquery(hellopreloader);
        },0);
      }
    }else{
      window.onload = function(){
        setTimeout(function(){
          fadeOutnojquery(hellopreloader);
        },0);
      }
    }
    
    $(this.menuItem).on('click', function () {
      let itemID = $(this).attr('href');
      // self.tl.play();
      if (itemID == '#main__services') {
        self.mainService()
      // } else if (itemID == '#main__about') {
      //   self.mainAbout()
      // } else if (itemID == '#main__news') {
      //   self.mainNews()
      // } else if (itemID == '#main__diagnosis') {
      //   self.mainDiagnosis()
      } else if (itemID == '#main__reviews') {
        self.mainTeam()
        //   self.mainReviews()
      } else if (itemID == '#main__team') {
        self.mainTeam()
      }
      
    })




    this.tl.set([this.serviceTitle, this.items], { y: 50, opacity: 0 })
      // .set(this.aboutImg, { opacity: 0 })
      // .set(this.aboutText, { x: -50, opacity: 0 })
      // .set(this.newsTitle, { y: 50, opacity: 0 })
      // .set(this.itemNews, { y: 50, opacity: 0 })
      // .set(this.diagnosisImg, { opacity: 0 })
      // .set(this.diagnosisTitle, { x: -50, opacity: 0 })
      // .set([this.reviewsTitle, this.reviewsSlider], { y: 50, opacity: 0 })
      .set([this.teamTitle, this.teams, /*this.teamSlider*/], { y: 50, opacity: 0 })
  }
  mainService() {
    this.tl
      .staggerTo(this.serviceTitle, .5, {
        ease: Power4.easeInOut,
        y: 0,
        opacity: 1,
      }, 0)
      .staggerTo(this.serviceLine, .2, {
        ease: Power4.easeInOut,
        className: "+=main__line--visible"
      }, 0)
      .staggerTo(this.items, 0.5, {
        ease: Power4.easeInOut,
        y: 0,
        opacity: 1,
      }, .1)
  }
  // mainAbout() {
  //   this.tl
  //     .staggerTo(this.aboutImg, .75, {
  //       ease: Power4.easeInOut,
  //       opacity: 1,
  //     }, 0)
  //     .staggerTo(this.aboutText, .5, {
  //       ease: Power4.easeInOut,
  //       x: 0,
  //       opacity: 1,
  //     }, 0.1)
  // }
  // mainNews() {
  //   this.tl
  //     .staggerTo(this.newsTitle, .5, {
  //       ease: Power4.easeInOut,
  //       y: 0,
  //       opacity: 1,
  //     }, 0)
  //     .staggerTo(this.newsLine, .2, {
  //       ease: Power4.easeInOut,
  //       className: "+=main__line--visible"
  //     }, 0)
  //     .staggerTo(this.itemNews, 1, {
  //       ease: Power4.easeInOut,
  //       y: 0,
  //       opacity: 1,
  //     }, .1)

  // }
  // mainDiagnosis() {
  //   this.tl
  //     .staggerTo(this.diagnosisTitle, .5, {
  //       ease: Power4.easeInOut,
  //       x: 0,
  //       opacity: 1,
  //     }, .2)
  //     .staggerTo(this.diagnosisModal, .5, {
  //       ease: Power4.easeInOut,
  //       className: "+=visible"
  //     }, .5)
  //     .staggerTo(this.diagnosisImg, .5, {
  //       ease: Power4.easeInOut,
  //       opacity: 1,
  //     }, .2)
  // }
  // mainReviews() {
  //   this.tl
  //     .staggerTo(this.reviewsTitle, .5, {
  //       ease: Power4.easeInOut,
  //       y: 0,
  //       opacity: 1,
  //     }, 0)
  //     .staggerTo(this.reviewsLine, .2, {
  //       ease: Power4.easeInOut,
  //       className: "+=main__line--visible"
  //     }, 0)
  //     .staggerTo(this.reviewsSlider, .2, {
  //       ease: Power4.easeInOut,
  //       y: 0,
  //       opacity: 1,
  //     }, 0)
  // }
  mainTeam() {
    this.tl
      .staggerTo(this.teamTitle, .5, {
        ease: Power4.easeInOut,
        y: 0,
        opacity: 1,
      }, 0)
      .staggerTo(this.teamLine, .2, {
        ease: Power4.easeInOut,
        className: "+=main__line--visible"
      }, 0)
      .staggerTo(this.teams, .5, {
        ease: Power4.easeInOut,
        y: 0,
        opacity: 1,
      }, .1)
      // .staggerTo(this.teamSlider, .2, {
      //   ease: Power4.easeInOut,
      //   y: 0,
      //   opacity: 1,
      // }, 0)
  }
}
